var gulp = require('gulp'),
  // js
  gulpWebpack = require('gulp-webpack'),
  webpack = require('webpack'),
  // css
  autoprefixer = require('gulp-autoprefixer'), // ベンダープレフィックスの自動付加
  sass = require('gulp-sass'),
  minifycss = require('gulp-clean-css'), // css圧縮
  csscomb = require('gulp-csscomb'), //プロパティ記述順序を最適化する
  sassGlob = require('gulp-sass-glob'),
  gcmq = require('gulp-group-css-media-queries'), // 散乱したメディアクエリの記述をまとめる
  // image
  spritesmith = require('gulp.spritesmith'),
  imagemin = require('gulp-imagemin'),
  jpegtran = require('imagemin-jpegtran'),
  pngquant = require('imagemin-pngquant'),
  //ブラウザシンク
  browser = require('browser-sync'),
  reload = browser.reload,
  // その他
  plumber = require('gulp-plumber'), // エラーでgulpが強制終了するのを見逃す
  notify = require('gulp-notify'), // エラーの内容を表示
  watch = require('gulp-watch'),
  rename = require('gulp-rename'),
  named = require('vinyl-named'), // ディレクトリ構成を維持する
  fs = require('fs'),
  path = require('path'),
  change = require('gulp-changed'), //変更されたファイルだけを処理してコンパイルを少し早くする
  SRC = './src/', //編集ファイルの場所
  noSRC = '!./src/', // 除外用
  PUBLIC = './public/'; //コンパイル先

/**
 * jsのコンパイル
 */
gulp.task('js', function() {
  return gulp
    .src(SRC + 'assets/js_uncomp/**/*.js', {
      base: SRC + 'assets/js_uncomp'
    })
    .pipe(change(PUBLIC + 'assets/js/'))
    .pipe(
      named(function(file) {
        //　ディレクトリ構成維持
        return file.relative.replace(/\.[^\.]+$/, '');
      })
    )
    .pipe(gulp.dest(PUBLIC + 'assets/js'))
    .pipe(
      plumber({
        // errorHandler: notify.onError({
        //   title: 'jsのコンパイル失敗してるよ！',
        //   message: '<%= error.message %>'
        // })
      })
    )
    .pipe(
      gulpWebpack({
        //watch: true,
        output: {
          filename: '[name].min.js' // 同名のminifyファイルを作成
        },
        plugins: [
          // 圧縮
          new webpack.optimize.UglifyJsPlugin({
            compress: {
              warnings: false
            }
          })
        ],
        module: {
          loaders: []
        }
      })
    )
    //.pipe(gulp.dest(PUBLIC + 'assets/js/min/')) // minifyファイルはjs/min/フォルダに生成
    .pipe(
      browser.reload({
        stream: true
      })
    );
  // .pipe(notify({
  //   title: 'jsをコンパイルしたよ。',
  //   message: new Date(),
  // }));
});

/**
 * ベンダープレフィックスの付与するバージョンを指定
 */
var AUTOPREFIXER_BROWSERS = [
  // @see https://github.com/ai/browserslist#browsers
  // 主要なブラウザの指定
  'last 2 version', // 主要ブラウザの最新2バージョン
  'ie >= 9', // IE9以上
  // 'Edge >= 12', // Edge12以上
  'iOS >= 8', // iOS8以上
  // 'Opera >= 23', // Opera23以上
  // 'Safari >= 7', // Safari7以上

  // Androidなどのマイナーなデバイスの指定
  'Android >= 4.4' // Android4.4以上
];

/**
 * `.scss`を`.css`にコンパイルします。
 * ベンダープレフィックスの付与、インデントやプロパティの整形、圧縮が実行されます。
 */
gulp.task('sass', function() {
  return (
    gulp
      .src(SRC + 'assets/sass/**/*.scss')
      .pipe(change(PUBLIC + 'assets/css/')) //cssだけ@importの依存関係でキャッシュが取れないのでコメントアウト
      .pipe(sassGlob()) // Sassの@importでglobを有効にする
      .pipe(sass().on('error', sass.logError))
      .pipe(
        plumber({
          errorHandler: notify.onError({
            title: 'sassのコンパイル失敗してるよ！',
            message: '<%= error.message %>'
          })
        })
      )
      .pipe(
        autoprefixer({
          // ベンダープレフィックスの自動付与
          browsers: AUTOPREFIXER_BROWSERS,
          grid: false // 現状のautoprefixerだとgridのベンダープレフィックスが不安定らしいのでfalse
        })
      )
      .pipe(gcmq()) // メディアクエリをまとめる
      .pipe(csscomb()) //プロパティ記述順序を最適化する
      .pipe(gulp.dest(PUBLIC + 'assets/css/')) // 未圧縮のcssを生成
      .pipe(minifycss()) // cssを圧縮
      //.pipe(
      //  rename({
      //    suffix: '.min'
      //  })
      //) // 圧縮したcssを同名+minにする
      //.pipe(gulp.dest(PUBLIC + 'assets/css/min/'))
      // .pipe(notify({
      //   title: 'Sassをコンパイルしたよ。',
      //   message: new Date(),
      // }))
      .pipe(
        browser.reload({
          stream: true
        })
      )
  );
});

// フォルダの取得
var getFolders = function(dir_path) {
  return fs.readdirSync(dir_path).filter(function(file) {
    return fs.statSync(path.join(dir_path, file)).isDirectory();
  });
};

/**
 * スプライト画像生成、
 */
gulp.task('sprite', function() {
  // 複数ファイルに対応
  var folders = getFolders(SRC + 'assets/image_uncomp/tmp_sprite/');
  folders.forEach(function(folder) {
    var spriteData = gulp
      .src(SRC + 'assets/image_uncomp/tmp_sprite/' + folder + '/*.png') // スプライト化したい画像をここに入れる
      .pipe(
        spritesmith({
          imgName: 'sp_' + folder + '.png', // スプライト画像
          imgPath: '../images/sp_' + folder + '.png', // 生成される CSS テンプレートに記載されるスプライト画像パス
          cssName: '_sprite-' + folder + '.scss', // 生成される CSS テンプレート
          cssFormat: 'scss', // フォーマット拡張子
          padding: 5, // 画像同士のpadding
          cssVarMap: function(sprite) {
            sprite.name = 'sprite-' + sprite.name; // 生成される CSS テンプレートに変数の一覧を記述
          }
        })
      );
    spriteData.img.pipe(gulp.dest(SRC + 'assets/image_uncomp')); // imgName で指定したスプライト画像の保存先
    return spriteData.css.pipe(gulp.dest(SRC + 'assets/sass/base')); // cssName で指定した CSS テンプレートの保存先
  });
});

/**
 * デベロップディレクトリの画像を圧縮、
 * 階層構造を維持したまま、リリースディレクトリに出力します。png,jpg,gif,svgに対応
 */
gulp.task('imagemin', function() {
  return gulp
    .src([
      SRC + 'assets/image_uncomp/**/*.{png,jpg,gif,svg}',
      noSRC + 'assets/image_uncomp/tmp_sprite/**/*.{png,jpg,gif,svg}'
    ])
    .pipe(
      plumber({
        errorHandler: notify.onError({
          title: '画像の圧縮が失敗してるよ！',
          message: '<%= error.message %>'
        })
      })
    )
    .pipe(change(PUBLIC + 'assets/images/'))
    .pipe(
      imagemin([
        pngquant({
          quality: '65-80', //pngクオリティの調節
          speed: 1,
          floyd: 0
        }),
        imagemin.jpegtran({
          quality: 85, //jpgクオリティの調節
          progressive: true
        }),
        imagemin.svgo(),
        imagemin.optipng(),
        imagemin.gifsicle()
      ])
    )
    .pipe(imagemin()) // ガンマ情報削除
    .pipe(gulp.dest(PUBLIC + 'assets/images/'))
    .pipe(
      browser.reload({
        stream: true
      })
    );
});

/**
 * html編集したらテストフォルダにコピーを置く
 */
gulp.task('html', function() {
  return gulp
    .src(SRC + '**/*.html')
    .pipe(change(PUBLIC + '**/*.html'))
    .pipe(gulp.dest(PUBLIC));
  // .pipe(notify({
  //   title: 'htmlの変更をパブリックに反映したよ',
  //   message: new Date(),
  // }))
});

/**
 * ローカルサーバーを起動します。
 */
gulp.task('server', function() {
  browser({
    server: {
      baseDir: PUBLIC,
      index: 'index.html'
    }
  });
});

gulp.task('reload', function() {
  browser.reload();
});


/**
 * デバッグ用タスク
 */
gulp.task("change", function() {//監視のデバッグタスク
  console.log("変更されました")
});

/**
 * watchタスクを指定// スプライトだけ_sprite.scsの生成の関係でwatchタスクに含みません。
 */

gulp.task('watch', function() {
  watch(
    [
      'src/**/*.html',
      'src/assets/sass/**/*.scss',
      'src/assets/js_uncomp/**/*.js',
      'src/assets/image_uncomp/*.{png,jpg,gif,svg}'
    ],
    function(event) {
      gulp.start(['sass'], ['html'], ['js'], ['imagemin'], ['reload']);
    }
  );

  var targets = [
    'src/**/*.html',
    'src/assets/sass/**/*.scss',
    'src/assets/js_uncomp/**/*.js',
    'src/assets/image_uncomp/*.{png,jpg,gif,svg}'
  ];
  var watcher = gulp.watch(targets, ['change']);
  watcher.on('change', function(evt) {
    // ターゲット内のものに変更があるかどうかとそのパスとタイプをコンソールに表示。デバッグ用
    console.log('file: ' + evt.path + ', ' + 'type: ' + evt.type);
  });
});

gulp.task('default', [
  'watch',
  'server',
  'reload',
  'html',
  'sass',
  'js',
  'sprite',
  'imagemin'
]);
