# gulpについて

下記記事を終わらせていることが前提
nodejsのインストール
https://qiita.com/akakuro43/items/600e7e4695588ab2958d

終わったら、gulpのインストール（※初回のみ）


```sh
$ npm install --g gulp-cli
```


作業するフォルダにコンソールで移動して
- gulpfile.js
- package.json
- src

上記3点を作業するフォルダにコピーして、
下記で初回のパッケージインストール（※プロジェクト間のソースの受け渡しで使うのはここから）

```sh
    npm install .
```


インストールが完了したら、作業する際は下記で実行

```sh
    gulp
```
で、実行。~~または~~


```sh
    gulp watch
```

~~で実行するとsrcディレクトリに新規追加したファイルの監視もする。~~

**※html及びsass/layautとsass/page内は適当なフアイルを入れてるので適宜でご利用ください。**


## 最新の修正箇所
**※2018/12/07**

- jsの変更検知が不安定なので修正。gulpでjsの検知できるように彩度調整しました。
- gulp-webpackの中の人がメンテナンスしてないようなので、来年落ち着いた頃にjsのコンパイル方法を見直そうと思います。



# このパッケージのフォルダ構成
```
-作業ディレクトリ（作業をするフォルダ）
    - public -htmlはフォルダで階層をつけてもsrc内で編集したらこのディレクトリに入ります
        -css（src/sassで編集したものがコンパイルされて入る）
          -min（src/sassで編集したものをminify化したもの）
        -images（src/image_uncompから圧縮された画像が入る）
        -js（src/js_uncompressedで編集したものがコンパイルされて入る）
        -min（src/js_uncompressedで編集したものをminify化したもの）
    -src
        -sass（編集するCSS　「$gulp sass」、または「$gulp」「$gulp watch」でウォッチ中にファイルの更新をすると自動コンパイル）
        -image_uncomp（圧縮したい画像ファイル（png,jpg,gif,svg）を入れるところ。「$gulp imagemin」、または「$gulp」「$gulp watch」でウォッチ中にファイルの更新をすると圧縮をかけてパブリックに移動）
          -tmp_sprite　（スプライト化したい画像（pngのみ）を単品で入れるところ。フォルダを作ってスプライト化したい画像をいれる。使い方は後述）
        -js_uncomp（jsの編集ファイル 「$gulp js」、「$gulp watch」でウォッチ中にファイルの更新をすると自動コンパイル）
```



# このパッケージの特徴

- 既存のcompassからスタイルだけを抜き取ってruby経由では無くlibsassに変更
- style.cssへの呼び出しが今までファイルごとのリンクだったのを、"page/**";などフォルダ内全てを指定するように変更
- メディアクエリを一つにまとめたり、プロパティ記述順序を最適化する
- ベンダープレフィックスは自動的に付与されます
- スプライト画像とレスポンシブなスタイルを自動生成 sp_○○（フォルダ名）.pngとして吐き出される。使い方は後述。GoogleのPageSpeed Insightsなどで評価を高めたい場合に小さい画像の呼び出しで何度もレスポンスが発生する場合はスプライトでまとめて呼び出すといいです。
- minファイルを一緒に吐き出すようにしました。（public/js/min、public/css/min）GoogleのPageSpeed Insightsなどで評価を高めたい場合はminの方から呼び出してください。（ブラウザの実行速度や表示速度が若干早くなるのでできたらいつもminから呼ぶようにした方がいいです）
- jsとcssはエラーの通知をコンソールとデスクトップにわかりやすく通知するようにしました
- モジュールバンドラーはwebpackを使用してますが、jsは一つにまとめずに個別に吐き出すようにしてます。
- gulp実行中は全てのファイルを対象とするのを、出力先のファイルと比べて変更があるものだけをコンパイルするようにして実行速度を早くしました


# 追加したmixinについて

## iehack

下記のように適応させたいIEのバージョンを呼び出して使えるmixin

```
    .foo {
        color: red;
        @include hack-ie(ie8) {
        color: red;
        }
        @include hack-ie(ie9-10) {
        color: blue;
        }
        @include hack-ie(ie10) {
        color: yellow;
        }
        @include hack-ie(ie11) {
        color: orange;
        }
    }
```

## ellipsisのline-clamp

下記のような呼び出しで複数行（例は3行）超えそうになったら文末に三点リーダーをつける。
IE対応版


```

    @include line-clamp(3);

```


# WordpressやCMSで利用する場合

## gulpfile.jsのPUBLICのパスを変更してください

gulpfile.js変更前

```js
  PUBLIC = './public/';//コンパイル先
```

gulpfile.js変更後

```js
  PUBLIC = './□□/wp-content/themes/○○/';//コンパイル先 □□WPの一番上の階層○○テーマ名
```

## gulpfile.jsのhtmlタスクとブラウザシンクの設定を変更

gulpfile.js変更前

```js
  /**
   * html編集したらテストフォルダにコピーを置く
   */
  gulp.task('html', function() {
    return gulp
      .src(SRC + '**/*.html')
      .pipe(change(PUBLIC + '**/*.html'))
      .pipe(gulp.dest(PUBLIC));
      .pipe(notify({
        title: 'htmlの変更をパブリックに反映したよ',
        message: new Date(),
      }))
  });

  /**
   * ローカルサーバーを起動します。
   */
  gulp.task('server', function() {
    browser({
      server: {
        baseDir: PUBLIC,
        index: 'index.html'
      }
    });
  });
```

gulpfile.js変更後

```js
  /**
   * html編集したらテストフォルダにコピーを置く
   */
  gulp.task('html', function() {// htmlは使わないのでコメントアウト
    // return gulp
    //   .src(SRC + '**/*.html')
    //   .pipe(change(PUBLIC + '**/*.html'))
    //   .pipe(gulp.dest(PUBLIC));
    // .pipe(notify({
    //   title: 'htmlの変更をパブリックに反映したよ',
    //   message: new Date(),
    // }))
  });

  /**
   * ローカルサーバーを起動します。
   */
  gulp.task('server', function() {
    browser.init({
      proxy: "http://127.0.0.1:8080/○○/wp/",// ip:アパッチが使ってるポート/フォルダ名/WPのおいてあるフォルダ名
      port: 8081//アパッチで使ってるポートじゃなくブラウザシンクに割り振るポート
    });
  });
```


## package.jsonのbrowser-syncのパスを変更してください

package.json変更前

```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "browser-sync start --server --files='./public/**/*.html, ./public/**/*.css, ./public/**/*.js'"
  },
```

package.json変更後

```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "browser-sync start --server --files='./□□/wp-content/themes/○○/, ./□□/wp-content/themes/○○/**/*.css, ./□□/wp-content/themes/○○/**/*.js'"
  },
```

## watchタスクのviewのパスをhtmlからphpに変更してください
htmlはsrcにおいたものを編集してるので、gulpfileで設定しているhtmlを実行
WordpressやCMSの場合、phpを直接編集した方が速そうなので、gulpfileにタスクを追加せずに直接書き換えることでライブリロードするようにしてます。
gulp-watchの不具合（？）かなにかで、パスの前に./があると正常に動かない場合があるみたいなので下記の書き方になります。
PUBLIC + '*.php'で指定しても動いたけど念のため。



gulpfile.js変更前

```js
gulp.task('watch', function(){
  watch(['src/**/*.html', 'src/sass/**/*.scss', 'src/js_uncomp/**/*.js', 'src/image_uncomp/*.{png,jpg,gif,svg}'], function(event){
      gulp.start(['sass'], ['html'], ['js'], ['imagemin'], ['reload']);
  });
});
```

gulpfile.js変更後

```js
gulp.task('watch', function(){
  watch(['□□/wp-content/themes/○○/*.php', 'src/sass/**/*.scss', 'src/js_uncomp/**/*.js', 'src/image_uncomp/*.{png,jpg,gif,svg}'], function(event){
      gulp.start(['sass'], ['js'],  ['imagemin'], ['reload']);
  });
});
```


# スプライト画像の生成と呼び出し
## image_uncomp/tmp_spriteにcommonというフォルダを作りlogoというpngを入れた場合

### スプライト画像生成
```sh
    gulp sprite
```
で image_tempにsp_common.pngが吐き出される。

### スプライト画像の圧縮

```sh
    gulp imagemin
```
で画像を圧縮。

### 使い方

```scss
    @include sprite-responsive($sprite-logo);
```

 という記述で呼び出し可能


### example

コンパイル前

```scss
    #logo {
        a {
            @include sprite-responsive($sprite-logo);
        }
    }
```

コンパイル後

``` css
#logo a {
    font-size: 0;

    overflow: hidden;

    white-space: nowrap;
    text-indent: 100%;

    background-image:url(../images/sp_common.png);
    background-size:505.6338% 811.42857%;
    background-position:96.35417% 64.25703%;
}
#logo a:after {
    display: block;

    padding-top: 80%;

    content: '';
}
```



### 補足

```sh
    gulp sprite
```
上記を実行するとsrc/sass/base/_sprite.scssが自動生成されるのでそれを元に
src/sass/mixin/_sprite-responsive.scssで、レスポンシブの調整をしている。
調整をする場合はsrc/sass/mixin/_sprite-responsive.scssを書き換える
