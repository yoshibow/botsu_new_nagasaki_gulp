
/*----------------------------------------
	index.js
----------------------------------------*/
var ww = $(window).width();
var wh = $(window).height();
var winScrollTop = 0;

/*----------------------------------------
	lottie
----------------------------------------*/
var device;
if(ww>690){
	device = 'pc';
}else{
	device = 'sp';
}
/*
const mv_top_anim = lottie.loadAnimation({
	container: document.getElementById('lottie_top'),
	renderer: 'svg',
	loop: false,
	autoplay: false,
	path: './assets/js/json/top_'+device+'.json'
});
*/
const mv_bottom_anim = lottie.loadAnimation({
	container: document.getElementById('lottie_bottom'),
	renderer: 'svg',
	loop: false,
	autoplay: false,
	path: '../assets/js/json/bottom_'+device+'.json'
});

/*----------------------------------------
	main visual
----------------------------------------*/
$(function(){
	var mv_offset = $(".p-index-top-area__main-visual h1").offset().top;
	var mv_heading = $(".p-index-top-area__main-visual h1");
	var concept_offset = $(".p-index-top-area__concept").offset().top;
	var dive_lady = $(".p-index-top-area__lady");
	
	$(window).on('scroll',function(){
		winScrollTop= $(window).scrollTop();
		if(winScrollTop > mv_offset/2){
			mv_heading.addClass("is_fade");
			dive_lady.addClass("is_active");
		}else{
			mv_heading.removeClass("is_fade");
			dive_lady.removeClass("is_active");
		}
		
		if(winScrollTop > concept_offset - wh){
			mv_bottom_anim.play();
		}
	});
	
	$(window).on("load", function(){
		setTimeout(function(){
			//mv_top_anim.play();
		},1000);
	});
	
	//APNG
	APNG.ifNeeded().then(function() {
		var images = document.querySelectorAll(".apng-image");
		for (var i = 0; i < images.length; i++) {
			APNG.animateImage(images[i]);
		}
	});
});