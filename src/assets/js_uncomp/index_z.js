
/*----------------------------------------
	index.js
----------------------------------------*/
var ww = $(window).width();
var wh = $(window).height();
var winScrollTop = 0;

/*----------------------------------------
	lottie
----------------------------------------*/
var device;
if(ww>690){
	device = 'pc';
}else{
	device = 'sp';
}
const mv_bottom_anim = lottie.loadAnimation({
	container: document.getElementById('lottie_bottom'),
	renderer: 'svg',
	loop: false,
	autoplay: false,
	path: '../assets/js/json/z_bottom_'+device+'.json'
});

/*----------------------------------------
	main visual
----------------------------------------*/
$(function(){
	var mv_offset = $(".p-index-top-area__main-visual h1").offset().top;
	var mv_heading = $(".p-index-top-area__main-visual h1");
	var concept_offset = $(".p-index-top-area__concept").offset().top;
	var dive_lady = $(".p-index-top-area__lady");
	
	$(window).on('scroll',function(){
		winScrollTop= $(window).scrollTop();
		if(winScrollTop > mv_offset/2){
			mv_heading.addClass("is_fade");
			dive_lady.addClass("is_active");
		}else{
			mv_heading.removeClass("is_fade");
			dive_lady.removeClass("is_active");
		}
		
		if(winScrollTop > concept_offset - wh){
			mv_bottom_anim.play();
		}
	});
	
	var parts_penguin_1 = $(".penguin_1");
	var parts_penguin_2 = $(".penguin_2");
	var parts_icecream_1 = $(".icecream_1");
	var parts_icecream_2 = $(".icecream_2");
	
	TweenMax.set([parts_penguin_1, parts_icecream_1], { x: "-100%", opacity: 0 });
	TweenMax.set([parts_penguin_2, parts_icecream_2], { x: "100%", opacity: 0 });
	
	//timelines
	function mv_tl(){
		const tl = new TimelineMax();
		tl.to(parts_penguin_1, 3, { x: "0%", opacity: 1, ease: Expo.easeOut })
		  .to(parts_penguin_2, 3, { x: "0%", opacity: 1, ease: Expo.easeOut }, "-=2.8")
		  .to(parts_icecream_1, 3, { x: "0%", opacity: 1, ease: Expo.easeOut }, "-=2.8")
		  .to(parts_icecream_2, 3, { x: "0%", opacity: 1, ease: Expo.easeOut }, "-=2.8")
	}
	
	$(window).on("load", function(){
		mv_tl();
	});
});

/*----------------------------------------
	concept
----------------------------------------*/
$(window).on("load", function(){
	$(window).scroll(function (){
		winScrollTop = $(this).scrollTop();
		
		$(".u-fadein-up").each(function(){
			var elemPos = $(this).offset().top;
			
			if (winScrollTop > elemPos - wh/1.2){
				gsap.to($(this), { duration: 4, y: 0, opacity: 1, ease: Expo.easeOut });
			}
		});
	});
});